### Hi 👋, I'm Sammy MERAZGA

I'm a 3rd year student pursuing an engineering degree at G4 institute :classical_building: that leads to an IT project manager title :mortar_board:. I am pursuing my study in alternation with Tempo One :city_sunset:.

I'm a passionate learner who's always ready to learn and work through technologies :bulb:. I love exploring new technologies and using them to solve real-world problems :sparkles:. I'm deep into Web :spider_web: Development.

![Snake animation](https://github.com/Platane/snk/raw/output/github-contribution-grid-snake.svg)

### Languages & Tools 🛠

![TypeScript](https://img.shields.io/badge/-typescript-05122A?style=flat&logo=typescript)&nbsp;
![JavaScript](https://img.shields.io/badge/-Javascript-05122A?style=flat&logo=javascript)&nbsp;
![HTML](https://img.shields.io/badge/-html5-05122A?style=flat&logo=html5)&nbsp;
![CSS](https://img.shields.io/badge/-css3-05122A?style=flat&logo=css3)&nbsp;
![PHP](https://img.shields.io/badge/-php-05122A?style=flat&logo=php)&nbsp;
![Java](https://img.shields.io/badge/-Java-05122A?style=flat&logo=Java&logoColor=white)&nbsp;

![NodeJS](https://img.shields.io/badge/-node.js-05122A?style=flat&logo=node.js)&nbsp;
![Express](https://img.shields.io/badge/-express-05122A?style=flat&logo=express)&nbsp;
![Symfony](https://img.shields.io/badge/-symfony-05122A?style=flat&logo=symfony)&nbsp;
![React](https://img.shields.io/badge/-react-05122A?style=flat&logo=react)&nbsp;
![MUI](https://img.shields.io/badge/-materialui-05122A?style=flat&logo=materialui)&nbsp;
![VueJS](https://img.shields.io/badge/-vue.js-05122A?style=flat&logo=vue.js)&nbsp;
![Vuetify](https://img.shields.io/badge/-vuetify-05122A?style=flat&logo=vuetify)&nbsp;
![Boostrap](https://img.shields.io/badge/-bootstrap-05122A?style=flat&logo=bootstrap)&nbsp;
![Quarkus](https://img.shields.io/badge/-quarkus-05122A?style=flat&logo=quarkus)&nbsp;

![Git](https://img.shields.io/badge/-Git-05122A?style=flat&logo=git)&nbsp;
![GitHub](https://img.shields.io/badge/-GitHub-05122A?style=flat&logo=github)&nbsp;

![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-05122A?style=flat&logo=postgresql)&nbsp;
![MySQL](https://img.shields.io/badge/-MySQL-05122A?style=flat&logo=mysql&logoColor=white)&nbsp;
![SQLite](https://img.shields.io/badge/-sqlite-05122A?style=flat&logo=sqlite)&nbsp;

![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-05122A?style=flat&logo=visual-studio-code&logoColor=007ACC)&nbsp;
![MAMP](https://img.shields.io/badge/-MAMP-05122A?style=flat&logo=MAMP)&nbsp;
![Postman](https://img.shields.io/badge/-Postman-05122A?style=flat&logo=postman)&nbsp;
![Yarn](https://img.shields.io/badge/-yarn-05122A?style=flat&logo=yarn)&nbsp;
![Linux](https://img.shields.io/badge/-Linux-05122A?style=flat&logo=linux&logoColor=white)&nbsp;
![MacOS](https://img.shields.io/badge/-MacOS-05122A?style=flat&logo=apple)&nbsp;

### Let's connect ? 🤝

<p align="left">
<a href="https://fr.linkedin.com/in/sammy-merazga"><img src="https://img.shields.io/badge/-sammyMERAZGA-0077B5?style=flat&logo=Linkedin&logoColor=white"/></a>
<a href="mailto:sammy.merazga@gmail.com"><img src="https://img.shields.io/badge/-sammy.merazga@gmail.com-D14836?style=flat&logo=Gmail&logoColor=white"/></a>
</p>

